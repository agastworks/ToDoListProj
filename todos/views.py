from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.views.generic import ListView, DetailView
# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todo_lists/list.html"
